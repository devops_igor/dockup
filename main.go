package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/devops_igor/dockup/helpers"
)

func main() {
	checkAll := flag.Bool("a", false, "Check updates for every repo on system on hub.docker.com")
	checkByName := flag.String("n", "", "Check update for given repo on hub.docker.com")
	//checkFromFile := flag.Bool("file", false, "Check updates for every image in images file")
	flag.Parse()
	flagsHandler(*checkAll, *checkByName)
}

func flagsHandler(a bool, n string) {
	if a {
		aHandler()
		os.Exit(0)
	}
	if n != "" {
		nHandler(n)
		os.Exit(0)
	}
	if !a && n == "" {
		fmt.Printf("Check -help for usage\n")
	}
}

func aHandler() {
	names := helpers.GetRepos()
	tags := helpers.GetTags()
	for i, name := range names {
		tag := tags[i]
		if strings.Contains(name, "/") {
			fmt.Printf(name + ":" + tags[i] + "\n")
			checkUpdates(name, tag)
		} else {
			libraryName := "library/" + name
			fmt.Printf(libraryName + ":" + tags[i] + "\n")
			checkUpdates(libraryName, tag)
		}
	}
}

func nHandler(name string) {
	tags := helpers.GetTag(name)
	if len(tags) == 0 {
		fmt.Println("Provided repo is not present on this machine")
		os.Exit(1)
	}
	for _, tag := range tags {
		if strings.Contains(name, "/") {
			fmt.Println(name + ":" + tag)
			checkUpdates(name, tag)
		} else {
			libraryName := "library/" + name
			fmt.Println(libraryName + ":" + tag)
			checkUpdates(libraryName, tag)
		}
	}

}

//Compare Local SHA and Remote SHA
func checkUpdates(name string, tag string) {
	token := helpers.GetToken(name)
	remoteSHA := helpers.RemoteSHA(name, token, tag)
	localSHA := helpers.LocalSHA(name, tag)
	if remoteSHA != "" {
		if remoteSHA == localSHA {
			fmt.Printf("No updates!\n\n")
		} else {
			fmt.Printf("Update available!\n\n")
		}
	} else {
		fmt.Println("Can not fetch Remote SHA maybe private repo")
	}

}
