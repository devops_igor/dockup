package helpers

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
)

var token Token
var sha Config

//Get token to access docker hub repo
func GetToken(name string) string {
	resp, _ := http.Get("https://auth.docker.io/token?scope=repository:" + name + ":pull&service=registry.docker.io")
	body, _ := io.ReadAll(resp.Body)
	defer resp.Body.Close()
	json.Unmarshal(body, &token)
	strToken := string(token.Bearer)
	return strToken
}

//Get Remote SHA of docker hub repo
func RemoteSHA(name string, token string, tag string) string {
	req, _ := http.NewRequest("GET", os.ExpandEnv("https://registry.hub.docker.com/v2/"+name+"/manifests/"+tag), nil)
	req.Header.Set("Accept", "application/vnd.docker.distribution.manifest.v2+json")
	req.Header.Set("Authorization", os.ExpandEnv("Bearer "+token))
	resp, _ := http.DefaultClient.Do(req)
	if resp.StatusCode == 401 {
		return ""
	}
	body, _ := io.ReadAll(resp.Body)
	resp.Body.Close()
	json.Unmarshal(body, &sha)
	strSha := string(sha.Config.Digest)
	//fmt.Printf("Remote repo: %s\n", strSha)
	return strSha
}
