package helpers

import (
	"os/exec"
	"strings"
)

//Get all repositories on local machine
func GetRepos() []string {
	cmdStr := "docker images --format {{.Repository}}"
	return cmdExecutor(cmdStr)
}

//Get tags for all repositories on local machine
func GetTags() []string {
	cmdStr := "docker images --format {{.Tag}}"
	return cmdExecutor(cmdStr)
}

//Get all tags for individual repository on local machine
func GetTag(name string) []string {
	cmdStr := "docker images " + name + " --format {{.Tag}}"
	return cmdExecutor(cmdStr)
}

//Get Local SHA
func LocalSHA(name string, tag string) string {
	// Strip "library/" from official images
	if strings.Contains(name, "library/") {
		name = strings.ReplaceAll(name, "library/", "")
	}
	cmdStr := "docker images -q --no-trunc " + name + ":" + tag
	cmd := exec.Command("/bin/sh", "-c", cmdStr)
	stdout, _ := cmd.Output()
	stdStr := strings.TrimSpace(string(stdout))
	//fmt.Printf("Local repo: %s\n", stdStr)
	return stdStr
}

//Execute given docker command on host machine
func cmdExecutor(cmdStr string) []string {
	cmd := exec.Command("/bin/sh", "-c", cmdStr)
	stdout, _ := cmd.Output()
	stdStr := string(stdout)
	returnStr := strings.Fields(stdStr)
	return returnStr
}
