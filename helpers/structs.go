package helpers

type Token struct {
	Bearer string `json:"token"`
}

type Config struct {
	Config Digest `json:"config"`
}

type Digest struct {
	Digest string `json:"digest"`
}
